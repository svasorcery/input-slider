import { Component, Input, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-input-slider',
  templateUrl: './input-slider.component.html',
  styleUrls: ['./input-slider.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => InputSliderComponent),
      multi: true
    }
  ]
})
export class InputSliderComponent implements ControlValueAccessor {
  public value: number = 0;

  @Input() label: string = '';
  @Input() placeholder = '';
  @Input() slider = true;
  @Input() min = 0;
  @Input() max = 10000000;
  @Input() step = 1;
  @Input() id = 'input';
  @Input() name = 'input';
  @Input() subtext = '';
  @Input() maxlength = 10;

  decimalPattern = new RegExp('[0-9]+([.][0-9]+)?');

  onChange = (value: number | null): void => {
    // const parse = parseFloat(value) || this.min;
    // this.value = parse;
    this.value = value ?? 0;
    this.propagateChange(+this.value);
  }

  /* --- ControlValueAccessor -- */
  writeValue = (obj: any) => {
    if (!obj) { return; }
    this.value = obj;
  }
  private propagateChange = (_: any) => { };
  registerOnChange = (fn: any) => this.propagateChange = fn;
  registerOnTouched = (fn: any) => { };
  setDisabledState = (isDisabled: boolean) => { };

}
