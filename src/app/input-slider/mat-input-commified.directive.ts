import { Directive, ElementRef, HostListener, Input, OnInit, forwardRef } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { MAT_INPUT_VALUE_ACCESSOR } from '@angular/material/input';

@Directive({
  selector: 'input[matInputCommified]',
  providers: [
    {
      provide: MAT_INPUT_VALUE_ACCESSOR,
      useExisting: MatInputCommifiedDirective
    },
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MatInputCommifiedDirective),
      multi: true,
    }
  ]
})
export class MatInputCommifiedDirective implements ControlValueAccessor, OnInit {
  private _value: string | null = '';

  @Input() label: string = '';
  @Input() placeholder = '';
  @Input() slider = true;
  @Input() min = 0;
  @Input() max = 10000000;
  @Input() step = 1;
  @Input() id = 'input';
  @Input() name = 'input';

  constructor(private elementRef: ElementRef<HTMLInputElement>) { }

  get value(): string | null {
    return this._value;
  }

  @Input('value')
  set value(value: string | null) {
    this._value = value;
    this.formatValue(value);
  }

  ngOnInit(): void {
    this.formatValue(this._value);
  }

  @HostListener('input', ['$event.target.value'])
  onInput(value: string): void {
    this._value = value.replace(/[^\d.-]/g, '');
    this.onChange(this._value);
  }

  @HostListener('blur')
  onBlur(): void {
    this.formatValue(this._value);
  }

  @HostListener('focus')
  onFocus(): void {
    this.unFormatValue();
  }


  private formatValue(value: string | null): void {
    if (value !== null) {
      this.elementRef.nativeElement.value = numberWithCommas(value);
    } else {
      this.elementRef.nativeElement.value = '';
    }
  }

  private unFormatValue(): void {
    const value = this.elementRef.nativeElement.value;
    this._value = value.replace(/[^\d.-]/g, '');
    if (value) {
      this.elementRef.nativeElement.value = this._value;
    } else {
      this.elementRef.nativeElement.value = '';
    }
  }

  onChange = (value: string): void => {
    this._value = value;
    this.propagateChange(this.value);
  }

  /* --- ControlValueAccessor -- */
  writeValue = (obj: any) => {
    if (!obj) { return; }
    this._value = obj;
    this.formatValue(this._value);
  }
  private propagateChange = (_: any) => { };
  registerOnChange = (fn: any) => this.propagateChange = fn;
  registerOnTouched = (fn: any) => { };
  setDisabledState = (isDisabled: boolean) => { };

}

export function numberWithCommas(x: string) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
}
