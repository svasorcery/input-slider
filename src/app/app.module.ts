import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSliderModule } from '@angular/material/slider';

import { AppComponent } from './app.component';
import { InputSliderComponent } from './input-slider/input-slider.component';
import { MatInputCommifiedDirective } from './input-slider/mat-input-commified.directive';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSliderModule
  ],
  declarations: [
    AppComponent,
    InputSliderComponent,
    MatInputCommifiedDirective
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
